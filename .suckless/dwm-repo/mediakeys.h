// Key codes can be found with 'xev' command.
#ifndef MEDIAKEYS_H
#define MEDIAKEYS_H
#define XK_VOL_UP 0x1008ff13
#define XK_VOL_DOWN 0x1008ff11
#define XK_VOL_MUTE 0x1008ff12
#define XK_MIC_MUTE 0x1008ffb2
#define XK_DISPLAY 0x1008ff59
#define XK_AUDIO_NEXT 0x1008ff17
#define XK_AUDIO_PREV 0x1008ff16
#define XK_AUDIO_STOP_START 0x1008ff14
#endif
